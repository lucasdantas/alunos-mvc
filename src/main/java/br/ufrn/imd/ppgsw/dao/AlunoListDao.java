package br.ufrn.imd.ppgsw.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import br.ufrn.imd.ppgsw.domain.Aluno;

@ApplicationScoped
public class AlunoListDao implements AlunoDao {
	
	private static List<Aluno> bancoAlunos;
	
	public AlunoListDao() {
		bancoAlunos = new ArrayList<Aluno>();
	}

	@Override
	public Aluno salvar(Aluno entidade) {
		bancoAlunos.add(entidade);
		return entidade;
	}

	@Override
	public Aluno buscar(Long id) {
		for (Aluno aluno : bancoAlunos) {
			if (id.equals(aluno.getId())) {
				return aluno;
			}
		}
		
		return null;
	}

	@Override
	public Collection<Aluno> todos() {
		return bancoAlunos;
	}

	@Override
	public void remover(Aluno entidade) {
		bancoAlunos.remove(entidade);
	}

}
