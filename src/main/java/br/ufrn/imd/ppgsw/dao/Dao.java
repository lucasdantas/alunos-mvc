package br.ufrn.imd.ppgsw.dao;

import java.util.Collection;

import br.ufrn.imd.ppgsw.domain.Entidade;

/**
 * Interface abstrata para a padronização de classes de acesso a dados.
 *
 * @param <T> elemento do tipo Entidade
 */
public interface Dao<T extends Entidade> {

	/**
	 * Persiste uma entidade no repositório de dados.
	 * @param entidade a ser persistida
	 * @return entidade persistida
	 */
	public T salvar(T entidade);

	/**
	 * Busca uma entidade no repositório de dados.
	 * @param id identificador chave para a busca
	 * @return entidade buscada
	 */
	public T buscar(Long id);

	/**
	 * Lista todas as entidades existentes no repositório de dados.
	 * @return uma coleção de entidade
	 */
	public Collection<T> todos();

	/**
	 * Remove uma entidade do repositório de dados.
	 * @param entidade a ser removida
	 */
	public void remover(T entidade);

}
