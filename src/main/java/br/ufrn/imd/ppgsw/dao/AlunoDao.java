package br.ufrn.imd.ppgsw.dao;

import br.ufrn.imd.ppgsw.domain.Aluno;

public interface AlunoDao extends Dao<Aluno> {

}
