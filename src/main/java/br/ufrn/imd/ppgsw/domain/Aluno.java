package br.ufrn.imd.ppgsw.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.ws.rs.FormParam;

@Entity
public class Aluno extends Entidade {

	@FormParam("matricula")
	private String matricula;

	@FormParam("nome")
	private String nome;

	@FormParam("email")
	private String email;

	@FormParam("projeto")
	private String projeto;
	
	@Override
	public String getId() {
		return matricula;
	}

	@Override
	public void setId(String id) {
		this.matricula = id;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getProjeto() {
		return projeto;
	}

	public void setProjeto(String projeto) {
		this.projeto = projeto;
	}

}
