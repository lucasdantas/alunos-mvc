package br.ufrn.imd.ppgsw;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("app")
public class AlunosMvcApplication extends Application {

}
