package br.ufrn.imd.ppgsw.controller;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.mvc.Models;
import javax.mvc.Viewable;
import javax.mvc.annotation.Controller;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import br.ufrn.imd.ppgsw.dao.AlunoDao;
import br.ufrn.imd.ppgsw.domain.Aluno;

@Controller
@Path("alunos")
@ApplicationScoped
public class AlunosController {
	
	@Inject
	private AlunoDao alunoDao;
	
	@Inject
    private Models models;
	
	@GET
	@Path("listar")
    public Viewable listar() {
		this.models.put("alunos", alunoDao.todos());
        return new Viewable("listaAlunos.jsp");
    }
	
	@POST
    @Path("salvar")
    public String salvar(@BeanParam Aluno aluno) {
		alunoDao.salvar(aluno);
 
        return "redirect:alunos/listar";
    }

}