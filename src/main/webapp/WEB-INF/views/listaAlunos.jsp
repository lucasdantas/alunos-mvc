<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>

		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        
        <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</head>
<body>
	<div class="container">
		<div class="panel panel-default" style="margin: 10px">
	        <div class="panel-heading">
	            <h1 class="panel-title">Lista de Alunos</h1>
			</div>
			<div class="panel-body">
				<form class="form-inline" action="salvar" method="POST" style="margin: 20px 0">
					
					<input type="text" class="form-control" placeholder="Matrícula"
					    name="matricula"/>
					
					<input type="text" class="form-control" placeholder="Nome"
					    name="nome"/>
					
					<input type="email" class="form-control" placeholder="Email"
					    name="email"/>
					    
					<input type="text" class="form-control" placeholder="Projeto"
					    name="projeto"/>
					    
					<button type="submit" class="btn btn-primary">Adicionar</button>
				</form>
			
	            <table class="table">
					<thead>
						<tr>
	                        <th>Matrícula</th>
	                        <th>Nome</th>
	                        <th>Email</th>
	                        <th>Projeto</th>
	                    </tr>
	                </thead>
	                <tbody>
	                    <c:forEach items="${alunos}" var="a" >
	                    	<tr>
							    <td>${a.matricula}</td>
							    <td>${a.nome}</td>
							    <td>${a.email}</td>
							    <td>${a.projeto}</td>	
							</tr>
						</c:forEach>
	                </tbody>
	            </table>
        	</div>
		</div>
	</div>
</body>
</html>